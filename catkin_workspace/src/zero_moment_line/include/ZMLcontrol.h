/* 
 * File:   quadruped_control.h
 * Author: alex
 *
 * Created on March 5, 2014, 6:08 PM
 */

#ifndef ZMLCONTROL_H
#define	ZMLCONTROL_H

#include <kdl/frames.hpp>
#include <vector>
#include <map>
#include <iostream>

class ZMLControl{
public:
    ZMLControl();
    virtual ~ZMLControl();
    void determineZML(const KDL::Wrench &desired_wrench, KDL::Vector &complementary_torque);
    void inputContactPoints(std::vector<KDL::Vector>& _contact_points);
    bool checkStability();
    void determineWeights(std::vector<double>& contact_weights);
    void sortPoints(const KDL::Vector& axis, const std::vector<KDL::Vector> &points, std::vector<KDL::Vector>& sorted_points, std::vector<int>& original_indices);
    std::vector<double> meanValueCoordinates(const KDL::Vector& _point, const std::vector<KDL::Vector>& vertices);
    
private:
    KDL::Vector ZML_origin;
    KDL::Vector ZML_direction;
    std::vector<KDL::Vector> contact_points;
    std::vector<KDL::Vector> projected_contact;
};


#endif	/* ZMLCONTROL_H */


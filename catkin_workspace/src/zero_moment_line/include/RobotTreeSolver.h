/* 
 * File:   RobotTreeSolver.h
 * Author: alex
 *
 * Created on March 5, 2014, 6:11 PM
 */

#ifndef ROBOTTREESOLVER_H
#define	ROBOTTREESOLVER_H

#include "kdl/treefksolver.hpp"
#include <vector>
#include <ros/ros.h>
#include <kdl/kdl.hpp>
#include <kdl/articulatedbodyinertia.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <Eigen/Core>


namespace KDL{

class RobotTreeSolver {
public:
    RobotTreeSolver(const Tree& _tree);
    virtual ~RobotTreeSolver();
    
    //set the joint name order for the robot
    int setInputFormat(const std::vector<std::string>& input_names);
    
    //update the solver with the current angles
    int updateSolver(const std::vector<double> &q_in);
    int getSegmentFrame(const std::string& segment_name, Frame& segment_frame);
    int getSegmentFrames(const std::vector<std::string>& segment_names, std::vector<Frame>& segment_frames);
    int getCOM(Eigen::Vector3d& com);
    int getCOM(Vector& com);
    int getSegmentJacobian(const std::string& segment_name, Eigen::MatrixXd& segment_jacobian);
    int getSegmentJacobians(const std::vector<std::string>& segment_names, std::vector<Eigen::MatrixXd>& segment_jacobians);
    void getCOMJacobian(Eigen::MatrixXd& CoMJacobian);
    int getCounterGravityTorques(const Vector& gravity, std::vector<double>& joint_torques);
    int getHMatrix(JntSpaceInertiaMatrix& Hmat, Eigen::MatrixXd& H_sup, ArticulatedBodyInertia& SysTerm); 
    
    //with angle inputs
    int getSegmentFrame(const std::vector<double> &q_in, const std::string& segment_name, Frame& segment_frame);
    int getSegmentFrames(const std::vector<double> &q_in, const std::vector<std::string>& segment_names, std::vector<Frame>& segment_frames);
    int getCOM(const std::vector<double> &q_in, Eigen::Vector3d& com);
    int getCOM(const std::vector<double> &q_in, Vector& com);
    int getSegmentJacobian(const std::vector<double> &q_in, const std::string& segment_name, Eigen::MatrixXd& segment_jacobian);
    int getSegmentJacobians(const std::vector<double> &q_in, const std::vector<std::string>& segment_names, std::vector<Eigen::MatrixXd>& segment_jacobians);
    int getCOMJacobian(const std::vector<double> &q_in, Eigen::MatrixXd& CoMJacobian);
    int getCounterGravityTorques(const std::vector<double> &q_in, const Vector& gravity, std::vector<double>& joint_torques);
    int getHMatrix(const std::vector<double> &q_in, JntSpaceInertiaMatrix& Hmat, Eigen::MatrixXd& H_sup, ArticulatedBodyInertia& SysTerm); 

    //run update SegmentJacobians first
    int getJacobian(const std::string& base_segment_name, const std::string& tip_segment_name, Eigen::MatrixXd& segment_jacobian);
    
    //run update solver first
    int getGravityWrench(const Vector& gravity, Wrench& at_root);
    int exertForce(const Wrench& at_point, const std::string& segment_name, const Vector& point_offset,
                std::vector<double>& resultant_torques, Wrench& at_root);

    //updates the kdl angle array given an input array, called by other functions, must be called after setInputFormat
    int updateAngles(const std::vector<double> &q_in);
    //updates the joint array given a kdl joint array
    void updateAngles(const JntArray &q_in);
    //updates the global frames, must be called after update angles
    void updateFrames();
    //updates the leaf inertia terms, must be called after update frames
    void updateLeafInertias();
    //updates the segment jacobians, must be called after update frames
    void updateSegmentJacobians();
    //updates the jacobian for the com, must be called after update segment jacobians and update leaf inertias
    void updateCOMJacobian();
    //updates the counter gravity terms, must be called after update leaf inertias
    void updateCGTerms();
    //update the H matrix
    void updateHMatrix();
    
    //print segment frames for all segments
    void printSegmentFrames();
    
    JntSpaceInertiaMatrix H;
    Eigen::MatrixXd H_supplement;
    ArticulatedBodyInertia SystemInertia;
    
private:
    Tree tree;
    //the tree elements ordered from root to leaf
    std::vector<TreeElement> segmentsOrdered;
    //the index of the parent of the tree element, corresponding to segmentsOrdered, parent of root is -1
    std::vector<int> segmentParents;
    //key: segment name, value: segmentsOrdered index of segment
    std::map<std::string, int> segmentMap;
    //frame of segment relative to the root, same order as segmentsOrdered
    std::vector<Frame> globalFrames;
    //the distal mass in the root frame for each segment, used in internal calculations
    Eigen::MatrixXd leafInertias;
    //vector of the jacobians for each segment frame, each jacobian has JointAngles.size() columns
    std::vector<Jacobian> segmentJacs;
    //the jacobian of the center of mass, JointAngles.size() columns
    Jacobian comJac;
    
    //vector to store the joint_names corresponding to the input setup
    std::vector<std::string> joint_names;
    //the kdl index corresponding to each input joint_name
    std::vector<int> joint_index;
    
    //kdl array storing all the joint positions in kdl order
    JntArray jointAngles;
    //key: joint name , value: kdl index
    std::map<std::string, int> jointMap;
    //vector that holds the term to be dot with gravity to obtain the counter_gravity torques. internal calculation
    std::vector<Vector> cgJointVectors;
    
    //the sum of the inertias past the segment in the segment frame
    std::vector<ArticulatedBodyInertia> Inertias;
    
    //orders the segments, called when constructed
    void orderSegments();
    //checks if a segment exists in the kdl tree
    bool checkSegment(const std::string& segment_name);
};
}

//turns and eigen vector3d into a kdl vector
KDL::Vector eigen2KDL(Eigen::Vector3d eigen);



#endif	/* ROBOTTREESOLVER_H */


/* 
 * File:   QuadrupedControl.h
 * Author: alex
 *
 * Created on March 13, 2014, 6:04 PM
 */

#ifndef SIMPLEQUADRUPEDCONTROLLER_H
#define	SIMPLEQUADRUPEDCONTROLLER_H

#include <iostream>
#include <cstdlib>
#include <ros/ros.h>
#include <kdl/kdl.hpp>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <kdl_conversions/kdl_msg.h>
#include <kdl_parser/kdl_parser.hpp>
#include <tf/transform_datatypes.h>
#include <eigen_conversions/eigen_kdl.h>
#include <simple_quadruped/SimpleQuadrupedState.h>
#include <simple_quadruped/SimpleQuadrupedCommand.h>
#include <simple_quadruped/simple_quadruped_characteristics.h>
#include <zero_moment_line/ZMLData.h>
#include <ZMLcontrol.h>
#include <RobotTreeSolver.h>

class SimpleQuadrupedController {
public:
    SimpleQuadrupedController(KDL::Tree& _tree);
    virtual ~SimpleQuadrupedController();
    
    void updateState(simple_quadruped::SimpleQuadrupedStateConstPtr state_msg);
    void setDesiredPosition(KDL::Vector& new_desired_position){desired_position = new_desired_position;};
    void setDesiredOrientation(KDL::Rotation& new_desired_orientation){desired_orientation = new_desired_orientation;};
    void setOrientationGain(double new_gain){orientation_gain = new_gain;};
    void setSpringGain(double new_gain){spring_gain = new_gain;};
    zero_moment_line::ZMLData getControl(simple_quadruped::SimpleQuadrupedCommand* command);
    void getErrors(KDL::Vector& position_error_to_pack, KDL::Vector& rotation_error_to_pack);
    
    KDL::RobotTreeSolver robot_solver;
    simple_quadruped::SimpleQuadrupedState robot_state;

private:
    KDL::Tree robot_tree;
    std::vector<std::string> joint_names;
    ZMLControl zml_control;
    KDL::Rotation desired_orientation;
    KDL::Vector desired_position;
    double orientation_gain, spring_gain;
    KDL::Vector rotation_error, position_error;
};

#endif	/* QUADRUPEDCONTROL_H */


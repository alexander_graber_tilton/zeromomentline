/* 
 * File:   quadruped_control.cpp
 * Author: alex
 *
 * Created on March 6, 2014, 3:06 PM
 */

#include <cstdlib>
#include <ros/ros.h>
#include <kdl/kdl.hpp>
#include <kdl_conversions/kdl_msg.h>
#include <kdl_parser/kdl_parser.hpp>
#include <simple_quadruped/SimpleQuadrupedState.h>
#include <RobotTreeSolver.h>
#include <ZMLcontrol.h>
#include <SimpleQuadrupedController.h>
#include <std_msgs/Float64.h>
#include <zero_moment_line/ErrorData.h>

using namespace std;

/*
 * 
 */
void stateCallback(const simple_quadruped::SimpleQuadrupedState::ConstPtr &state, SimpleQuadrupedController* _main_control){
    _main_control->updateState(state);
}
void heightCallback(const std_msgs::Float64ConstPtr &height, SimpleQuadrupedController* _main_control){
    KDL::Vector desired_position(0,0,height->data);
    _main_control->setDesiredPosition(desired_position);
}
void orientationGainCallback(const std_msgs::Float64ConstPtr &new_gain, SimpleQuadrupedController* _main_control){
    _main_control->setOrientationGain(new_gain->data);
}
void heightGainCallback(const std_msgs::Float64ConstPtr &new_gain, SimpleQuadrupedController* _main_control){
    _main_control->setSpringGain(new_gain->data);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "ZML_control");
    ros::NodeHandle node;

    double frequency = 1000; //frequency to run the node at
    ros::Rate timer(frequency);
    
    //load tree of the robot
    KDL::Tree robot_tree;
    ROS_INFO("Loading robot description from parameter server....");
    while(!kdl_parser::treeFromParam("robot_description", robot_tree)){
        timer.sleep();
    }
    ROS_INFO("Robot Description Loaded");
    SimpleQuadrupedController main_control(robot_tree);
    //Initial values (ground is at ~ 0.026)
    KDL::Vector initial_position(0.0, 0.0, 0.45);
    main_control.setDesiredPosition(initial_position);
    main_control.setSpringGain(10.0);
    main_control.setOrientationGain(1.5);


    ROS_INFO("Main Control Initialized");
    //set the joint names
    ros::Subscriber state_sub = node.subscribe<simple_quadruped::SimpleQuadrupedState>("/simple_quadruped/state", 1, boost::bind(stateCallback, _1, &main_control));
    ros::Subscriber height_sub = node.subscribe<std_msgs::Float64>("/simple_quadruped/desired_height", 1, boost::bind(heightCallback, _1, &main_control));
    ros::Subscriber orientation_gain_subcriber = node.subscribe<std_msgs::Float64>("/simple_quadruped/orientation_gain", 1, boost::bind(orientationGainCallback, _1, &main_control));
    ros::Subscriber height_gain_subscriber = node.subscribe<std_msgs::Float64>("/simple_quadruped/height_gain", 1, boost::bind(heightGainCallback, _1, &main_control));
    ros::Publisher command_pub = node.advertise<simple_quadruped::SimpleQuadrupedCommand>("/simple_quadruped/command" , 1);
    ros::Publisher zml_data_publisher = node.advertise<zero_moment_line::ZMLData>("/zero_moment_line/data" , 1);
    ros::Publisher error_data_publisher = node.advertise<zero_moment_line::ErrorData>("/quadruped_control/error" , 1);
    
    int size = 0;
    while (ros::ok() && (size == 0)){
        ros::spinOnce();
        size = main_control.robot_state.position.size();
    }
    
    int header_sequence = 0;
    zero_moment_line::ErrorData error_data_msg;
    KDL::Vector current_position_error, current_rotation_error;
    while (ros::ok()){
        //wait for data
        ros::spinOnce();

	if (ros::Time::now().toSec() < 1.0){
            ROS_WARN("Waiting for simulation");
	}else{
            //calculations
            simple_quadruped::SimpleQuadrupedCommand command_msg;
            zero_moment_line::ZMLData zml_data = main_control.getControl(&command_msg);
            zml_data.header.stamp = ros::Time::now();
            zml_data.header.seq = header_sequence++;
            
            error_data_msg.header.stamp = zml_data.header.stamp;
            error_data_msg.header.seq++;
            main_control.getErrors(current_position_error, current_rotation_error);
            tf::vectorKDLToMsg(current_position_error, error_data_msg.position_error);
            tf::vectorKDLToMsg(current_rotation_error, error_data_msg.rotation_error);
                    
            command_pub.publish(command_msg);
            zml_data_publisher.publish(zml_data);
            error_data_publisher.publish(error_data_msg);
            //sleep till next cycle
        }
        timer.sleep();
    }
    return 0;
}


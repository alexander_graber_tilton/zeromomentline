/* 
 * File:   ZMLcontrol.cpp
 * Author: alex
 *
 * Created on March 5, 2014, 6:07 PM
 */

#include <cstdlib>
#include <ZMLcontrol.h>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <math.h>
#include <ros/ros.h>

using namespace std;
const bool DEBUG = false;

//NOTE: ORIGIN OF THE COORDINATES IS THE COM

ZMLControl::ZMLControl() {
}

ZMLControl::~ZMLControl() {
}

void ZMLControl::determineZML(const KDL::Wrench& desired_wrench,
		KDL::Vector& complementary_torque) {
	ZML_direction = desired_wrench.force;
	double amplitude = ZML_direction.Normalize(0.0);
	complementary_torque = dot(desired_wrench.torque, ZML_direction)
			* ZML_direction;
	ZML_origin = -(desired_wrench.torque - complementary_torque) * ZML_direction
			/ amplitude;
}

void ZMLControl::inputContactPoints(std::vector<KDL::Vector> &_contact_points) {
	contact_points = _contact_points;
}

bool ZMLControl::checkStability() {
	bool intersects = false;
	//create triangles from points
	for (int i = 0; i < contact_points.size(); i++) {
		for (int j = i + 1; j < contact_points.size(); j++) {
			for (int k = j + 1; k < contact_points.size(); k++) {
				//the combination of points to try
				//i, j, k of contact_points
				//check if the line intersects with the triangle
				KDL::Vector v0, v1, v2, P1, u, v, w, n;
				double s1, t1;
				v0 = contact_points[i]; // origin of the triangle plane
				v1 = contact_points[j];
				v2 = contact_points[k];

				u = v1 - v0; //one edge of the triangle
				v = v2 - v0; //a second edge of the triangle
				n = u * v; //the normal of the triangle plane
				double denom = dot(n, ZML_direction);
				if (denom != 0) {
					double t = dot(n, (v0 - ZML_origin)) / denom;
					P1 = ZML_origin + t * ZML_direction;
					w = P1 - v0;
					denom = dot(u, v) * dot(u, v) - dot(u, u) * dot(v, v);
					if (denom != 0) {
						s1 = (dot(u, v) * dot(w, v) - dot(v, v) * dot(w, u))
								/ denom;
						t1 = (dot(u, v) * dot(w, u) - dot(u, u) * dot(w, v))
								/ denom;
						if ((s1 >= 0) && (t1 >= 0) && (s1 + t1 <= 1)) {
							intersects = true;
						}
					}
				}
			}
		}
	}
	return intersects;
}

void ZMLControl::determineWeights(std::vector<double>& contact_weights) {
	//project the contact points onto a plane normal to the ZML with an origin at the ZML origin
	projected_contact.clear();
	KDL::Vector current_point, projected_point;
	//std::cout << "Contact Points size: " << contact_points.size() << "\n";
	for (int i = 0; i < contact_points.size(); i++) {
		current_point = contact_points[i] - ZML_origin; //translate to frame origining at the zml_origin
		projected_point = current_point
				- dot(ZML_direction, current_point) * ZML_direction;
		projected_contact.push_back(projected_point);
	}
	//sort the projected contact points in clockwie order. as angle orientation of each doesn't matter, take the first point as a zero angle
	std::vector < KDL::Vector > sorted_points;
	std::vector<int> original_indices;
	sortPoints(ZML_direction, projected_contact, sorted_points,
			original_indices);

	//points have been sorted
	//points are all on the same plane with origin at ZML origin

	//use mean value coordinates to determine foot weights
	//these weights should be multiplied by the desired Net force to get the feet forces
	if (DEBUG) {
		std::cout << "sorted_points_size; " << sorted_points.size() << "\n";
	}
	std::vector<double> unaligned_contact_weights = (meanValueCoordinates(
			KDL::Vector::Zero(), sorted_points));
	std::vector<double> storage(contact_points.size(), 0.0);

	contact_weights.resize(unaligned_contact_weights.size(), 0.0);
	if (DEBUG) {
		std::cout << "unaligned_contact_weight_size; "
				<< unaligned_contact_weights.size()
				<< "\n contact_weight_size; " << contact_weights.size()
				<< "\n original_indices_size: " << original_indices.size()
				<< "\n";

	}

	int index;
	for (int i = 0; i < unaligned_contact_weights.size(); i++) {
		index = original_indices[i];
		if (DEBUG) {
			std::cout << "Original Index: " << index << "\n";
		}

		contact_weights[index] = unaligned_contact_weights[i];
		if (DEBUG) {
			std::cout << "Original_Index: " << index << " , weight: "
					<< unaligned_contact_weights[i] << "\n";
		}
	}
}

void ZMLControl::sortPoints(const KDL::Vector &axis,
		const std::vector<KDL::Vector>& points,
		std::vector<KDL::Vector>& sorted_points,
		std::vector<int>& original_indices) {
	sorted_points = points;
	int size = points.size();
	original_indices.resize(size, 0);
	for (int i = 0; i < size; i++) {
		original_indices[i] = i;
	}
	if (size < 3) {
		if (DEBUG) {
			std::cout << "Less than 3 points";
		}
		return; //if there are 2 or less points, they are already sorted
	}
	bool sorted = false;
	int count = 0;
	int index = 0, next_index = 0;
	int iter = 0;
	double trip_prod;
	KDL::Vector temp;
	int temp_original_index;
	while (!sorted) {
		next_index = (index + 1) % size;
		//tripple product axis * (first x second)
		trip_prod = dot(axis, sorted_points[index] * sorted_points[next_index]);
		//if trip_prod > 0 , then second is counterclockwise from first, so switch
		// switch the points, reset the count;
		
		if (trip_prod > 0) {
			temp = sorted_points[index];
			temp_original_index = original_indices[index];
			
			sorted_points[index] = sorted_points[next_index];
			original_indices[index] = original_indices[next_index];
			

			sorted_points[next_index] = temp;
			original_indices[next_index] = temp_original_index;
			count = 0;
			index += size - 2; //go back 1 (-2 +1) to propagate
		} else {
			//if angle is negative, leave alone, increase count;
			count++;
			//if the count is greater than the size, the list is fully sorted
		}
		index = (index + 1) % size;
		if (count > size) {
			sorted = true;
		}
		if (++iter > size * size * size) {
			//something is wrong, list cannot be sorted
			std::cout << "Sorting Failed!\n";
			break;
		}
	}

	if (DEBUG) {
		for (int i = 0; i < size; i++) {
			std::cout << sorted_points[i] << " , original_index: "
					<< original_indices[i] << "\n";
		}
	}
}

std::vector<double> ZMLControl::meanValueCoordinates(const KDL::Vector& _point,
		const std::vector<KDL::Vector>& vertices) {
//input must be sorted and on the same plane
	int size = vertices.size();
	std::vector<double> coordinates;
	double coord_sum = 0;

	KDL::Vector current_vertex, next_vertex, previous_vertex;
	int current_index, previous_index, next_index;
	double A, Ap, B, C;

	if (size > 2) {
		//get plane normal
		KDL::Vector plane_normal = KDL::Vector::Zero();
		for (current_index = 0; current_index < size; current_index++) {
			next_index = (current_index + 1) % size;
			current_vertex = vertices[current_index];
			next_vertex = vertices[next_index];
			plane_normal += current_vertex * next_vertex;
		}
		plane_normal.Normalize(1.0e-10);
		if (DEBUG) {
			std::cout << "plane normal: " << plane_normal << "\n";
		}

		//find the areas of the different sections
		std::vector<double> As, Bs, Cs;
		for (current_index = 0; current_index < size; current_index++) {
			previous_index = (current_index - 1 + size) % size;
			next_index = (current_index + 1) % size;
			current_vertex = vertices[current_index];
			previous_vertex = vertices[previous_index];
			next_vertex = vertices[next_index];

			Ap = dot((previous_vertex * current_vertex), plane_normal) / 2.0;
			A = dot((current_vertex * next_vertex), plane_normal) / 2.0;
			B = dot((previous_vertex * next_vertex), plane_normal) / 2.0;
			C = dot(
					((next_vertex - current_vertex)
							* (previous_vertex - current_vertex)), plane_normal)
					/ 2.0;
			As.push_back(A);
			Bs.push_back(B);
			Cs.push_back(C);
			if (DEBUG) {
				std::cout << current_vertex << " , A: " << A << ", B: " << B
						<< ", C: " << C << "\n"; //debug
			}
		}

		//need a check for As = 0 at any element, as then the coordinates are on the outside line, and only i and i+1 are weighted
		double current_weight;
		for (current_index = 0; current_index < size; current_index++) {
			previous_index = (current_index - 1 + size) % size;
			next_index = (current_index + 1) % size;

			current_weight = vertices[previous_index].Norm()
					* As[current_index];
			current_weight -= vertices[current_index].Norm()
					* Bs[current_index];
			current_weight += vertices[next_index].Norm() * As[previous_index];
			current_weight /= (As[previous_index] * As[current_index]);

			coord_sum += current_weight;
			coordinates.push_back(current_weight);
		}

		for (int i = 0; i < size; i++) {
			coordinates[i] /= coord_sum;
			if (DEBUG) {
				cout << vertices[i] << ", weight: " << coordinates[i] << "\n";
			}
		}
	}
	return coordinates;
}

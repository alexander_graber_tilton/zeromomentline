/* 
 * File:   QuadrupedControl.cpp
 * Author: alex
 * 
 * Created on March 13, 2014, 6:04 PM
 */
#include <SimpleQuadrupedController.h>
const bool DEBUG = false;

SimpleQuadrupedController::SimpleQuadrupedController(KDL::Tree& _tree) :
		robot_tree(_tree), robot_solver(_tree) {
	//get joint names from characteristics class
	joint_names = SimpleQuadrupedCharacteristics::joint_names;
	robot_solver.setInputFormat(joint_names);

	//default values for orientation and height
	desired_orientation = KDL::Rotation::Identity();
	desired_position = KDL::Vector(0.0, 0.0, 0.0);
	orientation_gain = 20.0;
	spring_gain = 10.0;
}

SimpleQuadrupedController::~SimpleQuadrupedController() {
}

void SimpleQuadrupedController::updateState(
		simple_quadruped::SimpleQuadrupedStateConstPtr state_msg) {
	robot_state = *state_msg;
}

zero_moment_line::ZMLData SimpleQuadrupedController::getControl(
		simple_quadruped::SimpleQuadrupedCommand* command) {
	//update robot state
	std::vector<double> joint_position(robot_state.position.begin(),
			robot_state.position.end());
	robot_solver.updateAngles(joint_position);
	robot_solver.updateFrames();
	robot_solver.updateLeafInertias();
	robot_solver.updateCGTerms();

	KDL::Rotation robot_orientation;
	tf::quaternionMsgToKDL(robot_state.orientation, robot_orientation);
	double gravity_attenuation = 0.8;
	KDL::Vector gravity = gravity_attenuation * (robot_orientation.Inverse() * KDL::Vector(0, 0, -9.81));

	//compute counter gravity forces required
	std::vector<double> cg_torques, torques_to_exert;
	KDL::Wrench gravity_wrench, wrench_to_exert;

	//enter the gravity vector and get the vector of torques each joint requires to counter the forces of gravity
	robot_solver.getCounterGravityTorques(gravity, cg_torques);

	//this gets the force and torque that gravity applies at the root
	robot_solver.getGravityWrench(gravity, gravity_wrench);
	wrench_to_exert = -1 * gravity_wrench; //oppose the force of gravity
	torques_to_exert = cg_torques;

	//cg_torques are the torques to keep the limbs stationary against gravity if the root is supported
	//additional torques must be added to support the root.

	//angular_spring to corrent orientation
	rotation_error = (desired_orientation * robot_orientation.Inverse()).GetRot();
	
	KDL::Vector reorient_torque = orientation_gain * rotation_error;
	wrench_to_exert.torque += reorient_torque; //the torque that should be exerted on the robot

	//angular damping, angular velocity is in the root/imu frame
	KDL::Vector angular_velocity = KDL::Vector(robot_state.angular_velocity.x,
			robot_state.angular_velocity.y, robot_state.angular_velocity.z);
	double orientation_damping = 0;
	KDL::Vector angular_damping = -1 * orientation_damping * angular_velocity;
	wrench_to_exert.torque += angular_damping;

	//linear spring
	KDL::Vector spring_force;
	KDL::Vector current_position(robot_state.root_position.x,
			robot_state.root_position.y, robot_state.root_position.z);
	position_error = (desired_position - current_position); //displacement in world frame
	spring_force = spring_gain * (robot_orientation.Inverse() * position_error); //gain * displacement in the root frame
	wrench_to_exert.force += spring_force;

	//determine the contact forces at the contact points, then propagate the forces through the robot to get the additional torques
	std::vector < KDL::Vector > contact_points;
	KDL::Frame temp;
	std::vector<int> contact_indices;
	KDL::Vector current_force;
	std::vector<double> weights(10, 0);

	//debug, print the vector to all segments
	//robot_solver.printSegmentFrames();

	tf::vectorMsgToKDL(robot_state.left_front_contact, current_force);
	if (current_force.Norm() > 0.01) {
		robot_solver.getSegmentFrame("left_front_foot", temp);
		contact_points.push_back(temp.p);
		contact_indices.push_back(0);
	}
	tf::vectorMsgToKDL(robot_state.right_front_contact, current_force);
	if (current_force.Norm() > 0.01) {
		robot_solver.getSegmentFrame("right_front_foot", temp);
		contact_points.push_back(temp.p);
		contact_indices.push_back(1);
	}
	tf::vectorMsgToKDL(robot_state.right_rear_contact, current_force);
	if (current_force.Norm() > 0.01) {
		robot_solver.getSegmentFrame("right_rear_foot", temp);
		contact_points.push_back(temp.p);
		contact_indices.push_back(3);
	}
	tf::vectorMsgToKDL(robot_state.left_rear_contact, current_force);
	if (current_force.Norm() > 0.01) {
		robot_solver.getSegmentFrame("left_rear_foot", temp);
		contact_points.push_back(temp.p);
		contact_indices.push_back(2);
	}
	KDL::Vector complementary_torque;
	zml_control.determineZML(wrench_to_exert, complementary_torque);

	zero_moment_line::ZMLData zml_data;
	zml_data.header.frame_id = "root";

	geometry_msgs::Wrench desired_wrench_msg;
	geometry_msgs::Vector3 complementary_torque_msg;
	tf::wrenchKDLToMsg(wrench_to_exert, desired_wrench_msg);
	tf::vectorKDLToMsg(complementary_torque, complementary_torque_msg);
	zml_data.complementary_torque = complementary_torque_msg;
	zml_data.desired_wrench = desired_wrench_msg;

	zml_control.inputContactPoints(contact_points);
	
	if (DEBUG){
		std::cout << "Number of contact points: " <<contact_points.size() << "\n";
	}
	if (contact_points.size() > 2) {

		std::vector < KDL::Vector > forces;
		zml_control.determineWeights(weights);
		if (DEBUG){
			std::cout<< "Weights\n";
		}
		
		forces.resize(weights.size(), KDL::Vector(0.0,0.0,0.0));
		for (int i = 0; i < weights.size(); i++) {
			//the force exterted by the limbs should be opposite the force that needs to be exerted on the robot
			forces[i] = -1 * wrench_to_exert.force * weights[i]; //force each limb should exert

			if (DEBUG){
				std::cout << contact_points[i] << " , " << weights[i] << "\n";
			}
			//check to make sure this doesn't give duplicates
			geometry_msgs::Point contact_point_msg;
			geometry_msgs::Vector3 contact_force_msg;
			tf::pointKDLToMsg(contact_points[i], contact_point_msg);
			tf::vectorKDLToMsg(forces[i], contact_force_msg);
			zml_data.contact_points.push_back(contact_point_msg);
			zml_data.contact_forces.push_back(contact_force_msg);

		}
		//determine the counter torques for contacts
		std::vector<double> temp_torques;
		KDL::Wrench root_result, temp_wrench, frame_wrench;
		std::string index_map[] = { "left_front_foot", "right_front_foot",
				"left_rear_foot", "right_rear_foot" };
		for (int i = 0; i < contact_indices.size(); i++) {
			temp_wrench.force = forces[i];
			temp_wrench.torque = KDL::Vector::Zero();
			//forces need to be rotated with respect to the segment frame, not the root rotation
			robot_solver.getSegmentFrame(index_map[contact_indices[i]], temp);
			frame_wrench = temp.M.Inverse() * temp_wrench;
			//then solve for the required torques
			robot_solver.exertForce(frame_wrench, index_map[contact_indices[i]],
					KDL::Vector::Zero(), temp_torques, root_result);
			for (int j = 0; j < torques_to_exert.size(); j++) {
				//add all the individual torques together
				torques_to_exert[j] += temp_torques[j];
			}
		}
		command->command = torques_to_exert;
	} else {
		command->command = std::vector<double>(12, 0);
	}
	return zml_data;
}

void SimpleQuadrupedController::getErrors(KDL::Vector& position_error_to_pack, KDL::Vector& rotation_error_to_pack){
	position_error_to_pack = position_error;
	rotation_error_to_pack = rotation_error;
}

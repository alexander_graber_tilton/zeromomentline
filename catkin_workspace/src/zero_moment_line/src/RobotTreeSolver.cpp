/* 
 * File:   RobotTreeSolver.cpp
 * Author: alex
 * 
 * Created on March 5, 2014, 6:11 PM
 */

#include "RobotTreeSolver.h"

KDL::Vector eigen2KDL(Eigen::Vector3d eigen){
    KDL::Vector temp;
    temp.x(eigen[0]);
    temp.y(eigen[1]);
    temp.z(eigen[2]);
    return temp;
}


namespace KDL{

RobotTreeSolver::RobotTreeSolver(const Tree& _tree):
tree(_tree)
{
    orderSegments();
    jointAngles=KDL::JntArray(tree.getNrOfJoints());
    SetToZero(jointAngles);
    cgJointVectors = std::vector<Vector>(tree.getNrOfJoints(), Vector::Zero());
    comJac = Jacobian(tree.getNrOfJoints());
    SetToZero(comJac);
    H = JntSpaceInertiaMatrix(tree.getNrOfJoints());
    H_supplement = Eigen::MatrixXd::Zero(6, tree.getNrOfJoints());
    Inertias = std::vector<ArticulatedBodyInertia>(tree.getNrOfSegments()+1, ArticulatedBodyInertia::Zero());
}

RobotTreeSolver::~RobotTreeSolver() {
}


void RobotTreeSolver::orderSegments(){
    segmentsOrdered.clear();
    segmentsOrdered.push_back(tree.getRootSegment()->second);
    segmentParents.clear();
    segmentParents.push_back(-1);
    for (int i = 0; i < (int) segmentsOrdered.size(); i++){
        for(int j=0;j<(int)segmentsOrdered[i].children.size();j++){
            segmentsOrdered.push_back(segmentsOrdered[i].children[j]->second);
            segmentParents.push_back(i);
        }
    }
    int size = segmentsOrdered.size();
    globalFrames.resize(size);
    leafInertias = Eigen::MatrixXd(4, size);
    segmentJacs = std::vector<Jacobian>(size, Jacobian(tree.getNrOfJoints()));
    for (int i = 0; i < size; i++){
        segmentMap[segmentsOrdered[i].segment.getName()] = i;
        jointMap[segmentsOrdered[i].segment.getJoint().getName()] = segmentsOrdered[i].q_nr;
    }
}

int RobotTreeSolver::setInputFormat(const std::vector<std::string>& input_names){
    joint_names.clear();
    joint_index.clear();
    for (int i = 0; i < (int) input_names.size(); i++){
        if (jointMap.find(input_names[i]) != jointMap.end()){
            joint_names.push_back(input_names[i]);
            joint_index.push_back(jointMap[input_names[i]]);
        }else{
            ROS_ERROR("KDL tree: Could not find joint '%s' in the KDL tree. Possible robot model mismatch?",input_names[i].c_str());
            return i+1;
        }
    }
    return 0;
}

int RobotTreeSolver::updateAngles(const std::vector<double>& q_in){
    if (q_in.size() != joint_index.size()){
        ROS_ERROR("Joint Input: incorrect number of joint values sent, did you set the Input Format correctly?");
        return -1;
    }else{
        KDL::SetToZero(jointAngles);
        for(int i=0;i<(int)joint_index.size();i++){
            jointAngles(joint_index[i]) = q_in[i];
        }
    }
    return 0;
}

void RobotTreeSolver::updateFrames(){
  // Resize if necessary
  if(globalFrames.size()!=segmentsOrdered.size()) globalFrames.resize(segmentsOrdered.size());
  // Compute root frame
  {
    const TreeElement& currentElement = segmentsOrdered[0];
    globalFrames[0] = segmentsOrdered[0].segment.pose(jointAngles(currentElement.q_nr));
  }
  // Update the remaining frames
  for(int i=1;i<(int)segmentsOrdered.size();i++)
  {
    const TreeElement& currentElement = segmentsOrdered[i];
    globalFrames[i] = globalFrames[segmentParents[i]] * segmentsOrdered[i].segment.pose(jointAngles(currentElement.q_nr));
  }
}

void RobotTreeSolver::updateLeafInertias(){
    for (int i = 0; i < (int) segmentsOrdered.size(); i++){
        leafInertias(3,i) = segmentsOrdered[i].segment.getInertia().getMass();
        KDL::Vector root_cog = (globalFrames[i])*(segmentsOrdered[i].segment.getInertia().getCOG());
        leafInertias.block<3,1>(0,i) = Eigen::Vector3d(root_cog.data) * leafInertias(3,i);
    }
    
    //root frame does not have inertias term
    //temp solution, set to a small value
    leafInertias(3,0) = .00001;
    
    //leaf inertia now holds the mass properties of the segments in the pelvis frame. (m*px, m*py, m*pz, m)
    //add each child to it's parents, from the leaf in (so a segment will have the sum of the mass properties of itself
    //as well as all of it's children, and their children as well.
    for (int i = (int) segmentsOrdered.size() -1; i >= 0; i--){
        int parentID = segmentParents[i];
        if (parentID != -1){
            leafInertias.col(parentID) += leafInertias.col(i); 
        }
    }
}

void RobotTreeSolver::updateSegmentJacobians(){
    SetToZero(segmentJacs[0]);
    for (int i = 0; i < (int) segmentsOrdered.size(); i++){
        if (segmentParents[i] != -1){
            changeRefPoint(segmentJacs[segmentParents[i]], globalFrames[i].p - globalFrames[segmentParents[i]].p , segmentJacs[i]);
        }
        segmentJacs[i].setColumn(segmentsOrdered[i].q_nr, globalFrames[i].M*segmentsOrdered[i].segment.getJoint().twist(1.0));
    }
}

void RobotTreeSolver::updateCOMJacobian(){
    updateSegmentJacobians();
    SetToZero(comJac);
    Jacobian segmentMassJac = comJac; //destination jacobian must be the same size
    for (int i = 0; i < (int)segmentsOrdered.size(); i++){
        Vector trans = globalFrames[i].M*segmentsOrdered[i].segment.getInertia().getCOG();
        changeRefPoint(segmentJacs[i], trans, segmentMassJac);
        comJac.data = comJac.data + segmentMassJac.data * segmentsOrdered[i].segment.getInertia().getMass();
    }
    comJac.data /= leafInertias(3,0); //total mass
}

void RobotTreeSolver::updateCGTerms(){
    Vector pm;
    //joint axis dot (radius*mass x gravity) can be rewritten as
    //gravity dot (joint axis x radius*mass)
    //have everything in the root frame
    for (int i = 0; i < (int) segmentsOrdered.size(); i++){
        pm = eigen2KDL(leafInertias.block<3,1>(0,i));
        pm -= globalFrames[i].p * leafInertias(3,i);
        cgJointVectors[segmentsOrdered[i].q_nr] = (globalFrames[i].M*segmentsOrdered[i].segment.getJoint().JointAxis())*pm;
    }
}

void RobotTreeSolver::updateHMatrix(){
    //KDL does not load the root segment inertia, so I have hardcoded the value in
    Inertias[0] = ArticulatedBodyInertia(.00001, Vector(0, 0, 0), RotationalInertia(0.000000001, 0, 0, 0.000000001, 0, 0.000000001));
    for (int i = 1; i < (int) segmentsOrdered.size(); i++){
        Inertias[i] = (segmentsOrdered[i].segment.getInertia());
    }

    Twist segment_acc, current_seg_acc;
    double k,l;
    Wrench F; //unit wrench (force at the joint required given acceleration of 1)
    for (int i = segmentsOrdered.size() -1; i > 0; i--){
        Inertias[segmentParents[i]] = Inertias[segmentParents[i]] + globalFrames[segmentParents[i]].Inverse() * globalFrames[i] * Inertias[i];
        //if the segment has a joint, fill in relevant h terms
        if (segmentsOrdered[i].segment.getJoint().getType()!=Joint::None){
            k = segmentsOrdered[i].q_nr;
            segment_acc = segmentsOrdered[i].segment.twist(k, 1.0);//acceleration of segment i in parent frame caused by segment joint
            segment_acc = (globalFrames[segmentParents[i]] * globalFrames[i].Inverse()).M * segment_acc; // acceleration of segment i in segment frame
            F = Inertias[i] * segment_acc; //force and torque caused by acceleration of the segment (at segment tip)
            
            int j = i;
            
            //H_sup is the resulting force at the root
            H_supplement.block<3,1>(0,k) = Eigen::Vector3d((globalFrames[i]*F).force.data);
            H_supplement.block<3,1>(3,k) = Eigen::Vector3d((globalFrames[i]*F).torque.data);
            //iterate through the tree and fill out relevant H terms
            while (j > 0){
                if (segmentsOrdered[j].segment.getJoint().getType()!=Joint::None){
                    l = segmentsOrdered[j].q_nr;
                    current_seg_acc = (globalFrames[segmentParents[j]] * globalFrames[j].Inverse()).M * segmentsOrdered[j].segment.twist(l, 1.0);
                    //dot with the resulted wrench at the current joint, in the current segment frame
                    H(k,l) = dot(current_seg_acc, (globalFrames[j].Inverse() * globalFrames[i]) * F);
                    H(l,k) = H(k,l);
                }
                j = segmentParents[j];
            }
        }
    }            
    SystemInertia = Inertias[0]; //the inertia of the system about the root frame
}

int RobotTreeSolver::updateSolver(const std::vector<double>& q_in){
    int err = updateAngles(q_in);
    if(err != 0) return err;
    updateFrames();
    updateLeafInertias();
    updateSegmentJacobians();
    updateCOMJacobian();
    updateCGTerms();
    updateHMatrix();
    return 0;
}

bool RobotTreeSolver::checkSegment(const std::string& segment_name){
    if(segmentMap.find(segment_name) == segmentMap.end()){
        ROS_ERROR("KDL Tree: segment not found, possible robot miss-match? Segment: %s", segment_name.c_str());
        return false;
    }
    return true;
}

int RobotTreeSolver::getSegmentFrame(const std::string& segment_name, Frame& segment_frame){
    if (checkSegment(segment_name)){
        segment_frame = globalFrames[segmentMap[segment_name]];
    }else{
        return -1;
    }
    return 0;
}

int RobotTreeSolver::getSegmentFrames(const std::vector<std::string>& segment_names, std::vector<Frame>& segment_frames){
    segment_frames.clear();
    for (int i = 0; i < (int) segment_names.size(); i++){
        if(checkSegment(segment_names[i])){
            segment_frames.push_back(globalFrames[segmentMap[segment_names[i]]]);
        }else{
            return -1;
        }    
    }
    return 0;
}

int RobotTreeSolver::getCOM(Eigen::Vector3d& com){
    com = leafInertias.block<3,1>(0,0) / leafInertias(3,0);
    return 0;
}

int RobotTreeSolver::getCOM(Vector& com){
    Eigen::Vector3d e_com = leafInertias.block<3,1>(0,0) / leafInertias(3,0);
    com = eigen2KDL(e_com);
    return 0;
}

int RobotTreeSolver::getSegmentJacobian(const std::string& segment_name, Eigen::MatrixXd& segment_jacobian){
    Jacobian segment_jac;
    if (checkSegment(segment_name)){
        segment_jac = segmentJacs[segmentMap[segment_name]];
    }else{
        return -1;
    }
    segment_jacobian = Eigen::MatrixXd::Zero(6, joint_index.size());
    for (int i = 0; i < (int) joint_index.size(); i++){
        segment_jacobian.col(i) = segment_jac.data.col(joint_index[i]);
    }
    return 0;
}

int RobotTreeSolver::getSegmentJacobians(const std::vector<std::string>& segment_names, std::vector<Eigen::MatrixXd>& segment_jacobians){
    segment_jacobians.clear();
    int err = 0;
    for (int i = 0; i < (int) segment_names.size(); i++){
        Eigen::MatrixXd segment_jac;
        err = getSegmentJacobian(segment_names[i], segment_jac);
        if (err!=0) return err;
        segment_jacobians.push_back(segment_jac);
    }
    return 0;
}

void RobotTreeSolver::getCOMJacobian(Eigen::MatrixXd& CoMJacobian){
    CoMJacobian = Eigen::MatrixXd(6, joint_index.size());
    for (int i = 0; i < (int) joint_index.size(); i++){
        CoMJacobian.col(i) = comJac.data.col(joint_index[i]);
    }
}

int RobotTreeSolver::getSegmentFrame(const std::vector<double>& q_in, const std::string& segment_name, Frame& segment_frame){
    int err = updateAngles(q_in);
    if (err != 0) return err;
    updateFrames();
    return getSegmentFrame(segment_name, segment_frame);
}

int RobotTreeSolver::getSegmentFrames(const std::vector<double>& q_in, const std::vector<std::string>& segment_names, std::vector<Frame>& segment_frames){
    int err = updateAngles(q_in);
    if (err != 0) return err;
    updateFrames();
    return getSegmentFrames(segment_names, segment_frames);
}

int RobotTreeSolver::getCOM(const std::vector<double>& q_in, Eigen::Vector3d& com){
    int err = updateAngles(q_in);
    if(err!=0) return err;
    updateFrames();
    updateLeafInertias();
    err =  getCOM(com);
    return err;
}

int RobotTreeSolver::getCOM(const std::vector<double>& q_in, Vector& com){
    int err = updateAngles(q_in);
    if(err!=0) return err;
    updateFrames();
    updateLeafInertias();
    err = getCOM(com);
    return err;
}

int RobotTreeSolver::getSegmentJacobian(const std::vector<double>& q_in, const std::string& segment_name, Eigen::MatrixXd& segment_jacobian){
    int err = updateAngles(q_in);
    if(err!=0) return err;
    updateFrames();
    updateSegmentJacobians();
    return getSegmentJacobian(segment_name, segment_jacobian);
}

int RobotTreeSolver::getSegmentJacobians(const std::vector<double>& q_in, const std::vector<std::string>& segment_names, std::vector<Eigen::MatrixXd>& segment_jacobians){
    int err = updateAngles(q_in);
    if(err!=0) return err;
    updateFrames();
    updateSegmentJacobians();
    return getSegmentJacobians(segment_names, segment_jacobians);
}

int RobotTreeSolver::getCOMJacobian(const std::vector<double>& q_in, Eigen::MatrixXd& CoMJacobian){
    int err = updateAngles(q_in);
    if(err!=0) return err;
    updateFrames();
    updateLeafInertias();
    updateSegmentJacobians();
    updateCOMJacobian();
    getCOMJacobian(CoMJacobian);
    return 0;
}

int RobotTreeSolver::getCounterGravityTorques(const Vector& gravity, std::vector<double>& joint_torques){
    joint_torques.clear();
    for (int i = 0; i < (int) joint_index.size(); i++){
        if(joint_index[i] >= (int) cgJointVectors.size()){
            ROS_ERROR("Error in counter gravity");
            return -1;
        }else{
            joint_torques.push_back(-1 * dot(gravity, cgJointVectors[joint_index[i]]));
        }
    }
    return 0;
}

int RobotTreeSolver::getCounterGravityTorques(const std::vector<double>& q_in, const Vector& gravity, std::vector<double>& joint_torques){
    int err = updateAngles(q_in);
    if (err!=0) return err;
    updateFrames();
    updateLeafInertias();
    err = getCounterGravityTorques(gravity, joint_torques);
    if (err!=0) return err;
    return 0;
}

int RobotTreeSolver::getGravityWrench(const Vector& gravity, Wrench& at_root){
    Vector mass_torque = eigen2KDL(leafInertias.block<3,1>(0,0)) * gravity;
    Vector mass_force = gravity * leafInertias(3,0);
    at_root = Wrench(mass_force, mass_torque);
    return 0;
}

int RobotTreeSolver::exertForce(const Wrench& wrench_to_exert, const std::string& segment_name, const Vector& point_offset, std::vector<double>& required_torques, Wrench& required_root_wrench){
    if(!checkSegment(segment_name)) return -1;
    Wrench base_wrench = wrench_to_exert;
    base_wrench = base_wrench.RefPoint(-1 * point_offset);
    //now force and torque around segment frame in segment frame
    std::vector<double> joint_torques(jointAngles.rows(), 0);
    
    //Tj = r x Fp + Tp , Fp and Tp are the force and torque exerted at the point
    //r = rp - rj
    //Tj = (rp - rj) x Fp + Tp
    //Tj =  (rp x Fp) + Tp  - (rj x Fp)
    
    //translate wrench to root frame
    int index = segmentMap[segment_name];
    required_root_wrench = -1 * (globalFrames[index] * base_wrench);
    //required_root_wrench.force = - Fp
    //required_root_wrench.torque = -(rp x Fp) - Tp
    
    /*
    std::cout << "\nOrigin Segment: " << segmentsOrdered[index].segment.getName()
                <<"\nSegment Origin: (" << globalFrames[index].p.x() <<", "<< globalFrames[index].p.y() << ", " << globalFrames[index].p.z() << ")"
                <<"\nForce at Point: (" << wrench_to_exert.force.x() <<", "<< wrench_to_exert.force.y() << ", " << wrench_to_exert.force.z() << ")"
                <<"\nTorque at Point: (" << wrench_to_exert.torque.x() <<", "<< wrench_to_exert.torque.y() << ", " << wrench_to_exert.torque.z() << ")"
                <<"\nForce on Segment(RootFrame): (" << required_root_wrench.force.x() <<", "<< required_root_wrench.force.y() << ", " << required_root_wrench.force.z() << ")"
                <<"\nTorque at Root(RootFrame): (" << required_root_wrench.torque.x() <<", "<< required_root_wrench.torque.y() << ", " << required_root_wrench.torque.z() << ")"
                <<"\n";
    //*/
    
    //get vector to joint origin in root frame
    //rj = globalFrames[current].p
    
    //torque at joint in root frame
    //Tj = rj * required_root_wrench.force - required_root_wrench.torque
    Vector torque_at_joint;
    
    //get axis in root frame
    //Axisj = globalFrames[parent[current]].M * segments[current].getJoint.getAxis
    //double torque = dot (axis, Tj)
    

    
    //with respect to the root
    Frame current_frame;
    Vector joint_axis;
    
    //iterate back to the root
    while (segmentParents[index] >= 0){
        //variables
        joint_axis = globalFrames[segmentParents[index]] * (segmentsOrdered[index].segment.getJoint().JointAxis());
        torque_at_joint = globalFrames[index].p * required_root_wrench.force - required_root_wrench.torque;
        
        //if the joint exist/is not fixed (not zero axis)
        if (joint_axis != KDL::Vector::Zero()){
            joint_torques[segmentsOrdered[index].q_nr] = dot(torque_at_joint, joint_axis);
        }

        /*debug, printing segment name, noint name, number, wrenches, axis, and generated torque
        std::cout << "\nSegment Name: " << segmentsOrdered[index].segment.getName()
                <<"\nJoint Name: " << segmentsOrdered[index].segment.getJoint().getName()
                <<"\nJoint Number: " << segmentsOrdered[index].q_nr;
        std::cout <<"\nJoint Axis: (" << joint_axis.x() << ", " << joint_axis.y() << ", " << joint_axis.z() << ")";
        std::cout <<"\nRoot Force: (" << at_root.force.x() << ", " << at_root.force.y() << ", " << at_root.force.z() << ")";
        std::cout <<"\nPoint Torque: (" << torque_at_joint.x() << ", " << torque_at_joint.y() << ", " << torque_at_joint.z() << ")";
        std::cout <<"\nDesired Torque: " << joint_torques[segmentsOrdered[index].q_nr] <<"\n"; 
        //*/
        
        index = segmentParents[index];
    }
    
    //assignment
    required_torques.clear();
    for (int i = 0; i < (int) joint_index.size(); i++){
        required_torques.push_back(joint_torques[joint_index[i]]);
    }
    return 0;
}

int RobotTreeSolver::getHMatrix(JntSpaceInertiaMatrix& Hmat, Eigen::MatrixXd& H_sup, ArticulatedBodyInertia& SysTerm){
    int size = joint_index.size();
    Hmat = JntSpaceInertiaMatrix(size);
    H_sup = Eigen::MatrixXd::Zero(6, size);
    for (int i = 0; i < size; i++){
        for (int j = i; j < size; j++){
            Hmat(i,j) = H(joint_index[i], joint_index[j]);
            Hmat(j,i) = Hmat(i,j);
        }
        H_sup.col(i) = H_supplement.col(joint_index[i]);
    }
    SysTerm = SystemInertia;
    return 0;
}

int RobotTreeSolver::getHMatrix(const std::vector<double>& q_in, JntSpaceInertiaMatrix& Hmat, Eigen::MatrixXd& H_sup, ArticulatedBodyInertia& SysTerm){
    int err = updateAngles(q_in);
    if (err!=0) return err;
    updateFrames();
    updateHMatrix();
    getHMatrix(Hmat, H_sup, SysTerm);
    return 0;
}

int RobotTreeSolver::getJacobian(const std::string& base_segment_name, const std::string& tip_segment_name, Eigen::MatrixXd& returned_jacobian){
    Jacobian base_segment_jac;
    Jacobian tip_segment_jac;
    Frame base_f;
    Vector to_tip;
    if (checkSegment(base_segment_name)){
        base_segment_jac = segmentJacs[segmentMap[base_segment_name]];
        base_f = globalFrames[segmentMap[base_segment_name]];
    }else{
        return -1;
    }
    
    if (tip_segment_name == std::string("CoM")){
        tip_segment_jac = comJac;
        getCOM(to_tip);
    }else if (checkSegment(tip_segment_name)){
        tip_segment_jac = segmentJacs[segmentMap[tip_segment_name]];
        to_tip = globalFrames[segmentMap[tip_segment_name]].p;
    }else {
        return -1;
    }
    
    Jacobian tip_wrt_base = base_segment_jac;
    base_segment_jac.changeRefPoint(to_tip - base_f.p);
    for (int i = 0; i < (int) tip_segment_jac.columns(); i++){
        tip_wrt_base.setColumn(i, base_f.M.Inverse() * (tip_segment_jac.getColumn(i) - base_segment_jac.getColumn(i)));
    }
    
    returned_jacobian = Eigen::MatrixXd::Zero(6, joint_index.size());
    for (int i = 0; i < (int) joint_index.size(); i++){
        returned_jacobian.col(i) = tip_wrt_base.data.col(joint_index[i]);
    }
    return 0;
}

    void RobotTreeSolver::printSegmentFrames(){
        KDL::Frame current_frame;
        Eigen::Matrix3d current_rotation;
        for (int i = 0; i < segmentsOrdered.size(); i++){
            std::cout << "Segment: " << segmentsOrdered[i].segment.getName() << "\n";
            current_frame = globalFrames[i];
            std::cout << "Vector: (" << current_frame.p.x() << ", " << current_frame.p.y() << ", " << current_frame.p.z() << ")\n";
            current_rotation = Eigen::Matrix3d(current_frame.M.data).transpose();
            std::cout << "Rotation:\n " << current_rotation << "\n\n";
        }
    }

}

/* 
 * File:   test_program.cpp
 * Author: alex
 *
 * Created on March 11, 2014, 3:05 PM
 */

#include <ros/ros.h>
#include <kdl/kdl.hpp>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <ZMLcontrol.h>

/*
 * 
 */
using namespace std;

void calculateTorqueFromForce(KDL::Vector& force, KDL::Vector& contact_point,
		KDL::Vector& resultant_torque) {
	resultant_torque = contact_point * force;
}

bool vectors_equal(std::string test_name,
		std::vector<KDL::Vector> expected_vectors,
		std::vector<KDL::Vector> test_vectors, bool verbose) {
	int expected_size = expected_vectors.size();
	bool test_passed = (expected_size == test_vectors.size());
	if (!test_passed) {
		if (verbose) {
			cout << test_name << "failed: error in number of vectors: "
					<< expected_size << "!=" << test_vectors.size() << "\n";
		}
		return test_passed;
	}
	for (int i = 0; i < expected_size; i++) {
		bool elements_equal = Equal(expected_vectors[i], test_vectors[i],
				1.0e-10);
		test_passed = test_passed & elements_equal;
		if (!elements_equal) {
			if (verbose) {
				cout << test_name << " failed at: " << i << ": "
						<< expected_vectors[i] << "!= " << test_vectors[i]
						<< "\n";
			}
			return test_passed;
		}
	}
	return test_passed;
}

///////////////////////////////////////////////////////
// TESTS FROM HERE 
////////////////////////////////////////////////
bool testZMLCalculations(std::string test_name,
		std::vector<KDL::Vector> contact_points,
		KDL::Wrench desired_wrench) {
	ZMLControl zml_control;

	KDL::Vector complementary_torque;
	zml_control.determineZML(desired_wrench, complementary_torque);
	zml_control.inputContactPoints(contact_points);

	vector<double> weights;
	zml_control.determineWeights(weights);
	
	std::vector < KDL::Vector
			> control_forces(contact_points.size(), KDL::Vector::Zero());
	std::vector < KDL::Vector
			> resultant_torques(contact_points.size(), KDL::Vector::Zero());

	//determine total force and torque from the calculated forces
	KDL::Wrench total_wrench = KDL::Wrench(KDL::Vector::Zero(),
			complementary_torque);
	for (int i = 0; i < contact_points.size(); i++) {
		control_forces[i] = weights[i] * desired_wrench.force;
		calculateTorqueFromForce(control_forces[i], contact_points[i],
				resultant_torques[i]);
		total_wrench += KDL::Wrench(control_forces[i], resultant_torques[i]);
	}

	bool test_passed = Equal(desired_wrench, total_wrench, 1.0e-10);
	if (!test_passed) {
		std::cout << test_name
				<< " failed, total wrench does not equal desired wrench"
				<< "\nDesired wrench: " << desired_wrench << "\nTotal wrench: "
				<< total_wrench << "\n";
	}
	return test_passed;
}

bool testVectorEqualTest() {
	std::string test_name = "vector equals test";
	std::vector < KDL::Vector > expected_vectors;
	std::vector < KDL::Vector > test_vectors;

	//test input
	expected_vectors.push_back(KDL::Vector(-1, 1, 0));
	expected_vectors.push_back(KDL::Vector(1, 1, 0));
	expected_vectors.push_back(KDL::Vector(0, -1, 0));

	test_vectors.push_back(KDL::Vector(-1, 1, 0));
	test_vectors.push_back(KDL::Vector(1, 1, 0));
	test_vectors.push_back(KDL::Vector(0, -1, 0));

	bool test_passed = true;
	test_passed = test_passed
			& vectors_equal(test_name, expected_vectors, test_vectors, false);
	test_vectors[1] = KDL::Vector(1, 1, 4);
	test_passed = test_passed
			& !vectors_equal(test_name, expected_vectors, test_vectors, false);

	if (!test_passed) {
		std::cout << test_name << " failed\n";
	}
	return test_passed;
}

bool testSortedPoints() {
	std::string test_name = "sorting test 1";
	ZMLControl zml_control;
	std::vector < KDL::Vector > unsorted_points;
	std::vector < KDL::Vector > expected_order;
	std::vector < KDL::Vector > sorted_points;
	std::vector < int > original_indices;

	//test input
	unsorted_points.push_back(KDL::Vector(-1, 1, 0));
	unsorted_points.push_back(KDL::Vector(1, 1, 0));
	unsorted_points.push_back(KDL::Vector(0, -1, 0));
	KDL::Vector sort_axis = KDL::Vector(0, 0, 1);

	//test output
	expected_order.push_back(KDL::Vector(-1, 1, 0));
	expected_order.push_back(KDL::Vector(1, 1, 0));
	expected_order.push_back(KDL::Vector(0, -1, 0));
	int expected_size = expected_order.size();

	//run
	zml_control.sortPoints(sort_axis, unsorted_points, sorted_points, original_indices);
	if (!vectors_equal(test_name, expected_order, sorted_points, true)) {
		std::cout << test_name << "failed\n";
		return false;
	}
	return true;
}

bool testReverseSortedPoints() {
	std::string test_name = "sorting test 2: reverse sort";
	ZMLControl zml_control;
	std::vector < KDL::Vector > unsorted_points;
	std::vector < KDL::Vector > expected_order;
	std::vector < KDL::Vector > sorted_points;
	std::vector < int > original_indices;

	//test input
	unsorted_points.push_back(KDL::Vector(-1, 1, 0));
	unsorted_points.push_back(KDL::Vector(0, -1, 0));
	unsorted_points.push_back(KDL::Vector(1, 1, 0));
	KDL::Vector sort_axis = KDL::Vector(0, 0, 1);

	//test output
	expected_order.push_back(KDL::Vector(0, -1, 0));
	expected_order.push_back(KDL::Vector(-1, 1, 0));
	expected_order.push_back(KDL::Vector(1, 1, 0));
	int expected_size = expected_order.size();

	//run
	zml_control.sortPoints(sort_axis, unsorted_points, sorted_points, original_indices);
	if (!vectors_equal(test_name, expected_order, sorted_points, true)) {
		std::cout << test_name << "failed\n";
		std::cout << "sorted_points\n";
		for (int i = 0; i < sorted_points.size(); i++) {
			std::cout << sorted_points[i] << "\n";
		}
		return false;
	}
	return true;
}

bool testSimpleThreePoints() {
	std::string test_name = "three point weight test";
	std::vector < KDL::Vector > contact_points;
	//test points
	contact_points.push_back(KDL::Vector(-1, 1, 0));
	contact_points.push_back(KDL::Vector(1, 1, 0));
	contact_points.push_back(KDL::Vector(0, -1, 0));

	KDL::Vector desired_force = KDL::Vector(0, 0, 40);
	KDL::Vector desired_torque = KDL::Vector::Zero();
	//presort, so relations hold

	//test points
	bool test_passed = testZMLCalculations(test_name, contact_points,
			KDL::Wrench(desired_force, desired_torque));
	if (!test_passed){
		std::cout<< test_name << " failed\n";
	}
	return test_passed;
}

bool testSimpleFourPoints() {
	std::string test_name = "four point weight test";
	std::vector < KDL::Vector > contact_points;
	//test points
	contact_points.push_back(KDL::Vector(0, 1, 0));
	contact_points.push_back(KDL::Vector(0, -1, 0));
	contact_points.push_back(KDL::Vector(1, 0, 0));
	contact_points.push_back(KDL::Vector(-1, 0, 0));

	KDL::Vector desired_force = KDL::Vector(0, 0, 40);
	KDL::Vector desired_torque = KDL::Vector::Zero();
	//presort, so relations hold

	//test points
	bool test_passed = testZMLCalculations(test_name, contact_points,
			KDL::Wrench(desired_force, desired_torque));
	if (!test_passed){
		std::cout<< test_name << " failed\n";
	}
	return test_passed;
}

bool testSimpleFourPoints2() {
	std::string test_name = "four point weight test 2";
	std::vector < KDL::Vector > contact_points;
	//test points
	contact_points.push_back(KDL::Vector(1, 1, 0));
	contact_points.push_back(KDL::Vector(1, -1, 0));
	contact_points.push_back(KDL::Vector(-1, 1, 0));
	contact_points.push_back(KDL::Vector(-1, -1, 0));

	KDL::Vector desired_force = KDL::Vector(0, 0, 40);
	KDL::Vector desired_torque = KDL::Vector::Zero();
	//presort, so relations hold

	//test points
	bool test_passed = testZMLCalculations(test_name, contact_points,
			KDL::Wrench(desired_force, desired_torque));
	if (!test_passed){
		std::cout<< test_name << " failed\n";
	}
	return test_passed;
}

bool testNonZPlaneFourPoints() {
	std::string test_name = "non-Z plane four point weight test";
		std::vector < KDL::Vector > contact_points;
		//test points
		contact_points.push_back(KDL::Vector(1, 1, 1));
		contact_points.push_back(KDL::Vector(1, -1, 1));
		contact_points.push_back(KDL::Vector(-1, 1, -1));
		contact_points.push_back(KDL::Vector(-1, -1, -1));

		KDL::Vector desired_force = KDL::Vector(0, 0, 40);
		KDL::Vector desired_torque = KDL::Vector::Zero();
		//presort, so relations hold

		//test points
		bool test_passed = testZMLCalculations(test_name, contact_points,
				KDL::Wrench(desired_force, desired_torque));
		if (!test_passed){
			std::cout<< test_name << " failed\n";
		}
		return test_passed;
}

bool testComplexFourPoints() {
	std::string test_name = "complex four point weight test";
	std::vector < KDL::Vector > contact_points;
	//test points
	contact_points.push_back(KDL::Vector(0.227233476785, 0.11211039083, -0.0065792200361));
	contact_points.push_back(KDL::Vector(0.222999736191, -0.112122670191, -0.00658044335254));
	contact_points.push_back(KDL::Vector(-0.227230237966, -0.112114621344, -0.00655986169956));
	contact_points.push_back(KDL::Vector(-0.222998820592, 0.11212388899, -0.00657990381531));


	KDL::Vector desired_force = KDL::Vector(0, 0, 40);
	KDL::Vector desired_torque = KDL::Vector::Zero();
	//presort, so relations hold

	//test points
	bool test_passed = testZMLCalculations(test_name, contact_points,
			KDL::Wrench(desired_force, desired_torque));
	if (!test_passed){
		std::cout<< test_name << " failed\n";
	}
	return test_passed;
}

int main(int argc, char** argv) {
	bool all_tests_status = true;
//	all_tests_status &= testVectorEqualTest();
//	all_tests_status &= testSortedPoints();
//	all_tests_status &= testReverseSortedPoints();
//	all_tests_status &= testSimpleThreePoints();
//	all_tests_status &= testSimpleFourPoints();
//	all_tests_status &= testSimpleFourPoints2();
	all_tests_status &= testNonZPlaneFourPoints();
//	all_tests_status &= testComplexFourPoints();
	if (all_tests_status){
		std::cout << "ALL TESTS PASSED\n";
	}else{
		std::cout << "A TEST FAILED\n";
	}
	return 0;
}

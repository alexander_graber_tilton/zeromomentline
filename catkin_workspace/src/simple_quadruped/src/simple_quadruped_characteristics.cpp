#include <simple_quadruped/simple_quadruped_characteristics.h>

const int number_of_joints = 12;
const std::string joint_name_array[number_of_joints] = 
{"left_shoulder_x",
"left_shoulder_y",
"left_elbow",
"right_shoulder_x",
"right_shoulder_y",
"right_elbow",
"left_hip_x",
"left_hip_y",
"left_knee",
"right_hip_x",
"right_hip_y",
"right_knee"};

const std::vector<std::string> SimpleQuadrupedCharacteristics::joint_names(
		joint_name_array, joint_name_array + number_of_joints);

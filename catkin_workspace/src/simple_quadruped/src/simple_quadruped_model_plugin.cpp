#include <simple_quadruped/simple_quadruped_model_plugin.h>

namespace gazebo {
//Constructor
SimpleQuadrupedModelPlugin::SimpleQuadrupedModelPlugin() {
	std::string name = "simple_quadruped_model";
	int argc = 0;
	ros::init(argc, NULL, name);

	//Joint Names
	joint_names = SimpleQuadrupedCharacteristics::joint_names;

	//sensor placement references
	this->imuLinkName = "root";

	contactSensorPlacements.push_back("left_radius::left_front_contact");
	contactSensorPlacements.push_back("right_radius::right_front_contact");
	contactSensorPlacements.push_back("left_tibia::left_rear_contact");
	contactSensorPlacements.push_back("right_tibia::right_rear_contact");
}

//Destructor
SimpleQuadrupedModelPlugin::~SimpleQuadrupedModelPlugin() {
	delete this->node;
}

void SimpleQuadrupedModelPlugin::Load(physics::ModelPtr _parent,
		sdf::ElementPtr _sdf) {
	this->node = new ros::NodeHandle("simple_quadruped");
	this->model = _parent;
	InitializeJoints();

//	//listen for updates from gazebo
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&SimpleQuadrupedModelPlugin::OnUpdate, this));
//
//	//publish states over ros
	this->statePublisher = this->node->advertise
			< simple_quadruped::SimpleQuadrupedState
			> ("/simple_quadruped/state", 1);
	robot_state.position = std::vector<double>(12, 0);
	robot_state.velocity = std::vector<double>(12, 0);
	robot_state.torque = std::vector<double>(12, 0);
	robot_command.command = std::vector<double>(12, 0);

	//subscribe to commands through ros
	this->commandSubscriber = this->node->subscribe("/simple_quadruped/command",
			1, &SimpleQuadrupedModelPlugin::commandCallback, this);

	//find the imu sensor
	std::string scopedModelName = this->model->GetWorld()->GetName() + "::"
			+ this->model->GetScopedName();

	this->imuSensor = boost::dynamic_pointer_cast < sensors::ImuSensor
			> (sensors::SensorManager::Instance()->GetSensor(
					scopedModelName + "::root::imu_sensor"));

	if (!this->imuSensor) {
		gzerr << "imu_sensor not found\n" << "\n";
	}

	contactSensors.clear();
	for (unsigned int i = 0; i < contactSensorPlacements.size(); i++) {
		contactSensors.push_back(
				boost::dynamic_pointer_cast < sensors::ContactSensor
						> (sensors::SensorManager::Instance()->GetSensor(
								scopedModelName + "::"
										+ contactSensorPlacements[i])));
		if (!this->contactSensors[i]) {
			printf("%s not found\n", contactSensorPlacements[i].c_str());
		} else {
			forces.push_back(math::Vector3());
			torques.push_back(math::Vector3());
		}
	}
	printf("%lu contact sensors found\n", contactSensors.size());
}

void SimpleQuadrupedModelPlugin::OnUpdate() {
	robot_state.header.stamp = ros::Time::now();
	robot_state.header.frame_id = this->imuLinkName;

	//update joint poisitions and set the force for the timestep
	physics::JointPtr current_joint;
	for (int i = 0; i < joint_names.size(); i++) {
		current_joint = model->GetJoint(joint_names[i]);
		robot_state.position[i] = current_joint->GetAngle(0).Radian();
		robot_state.velocity[i] = current_joint->GetVelocity(0);
		robot_state.torque[i] = robot_command.command[i];
		current_joint->SetForce(0, robot_command.command[i]);
	}
	//get contacts
	for (unsigned int i = 0; i < contactSensors.size(); i++) {
		msgs::Contacts contactMessage = contactSensors[i]->GetContacts();
		sumContacts(contactMessage, forces[i], torques[i]);
	}

	//update robot state
	robot_state.left_front_contact.x = forces[0].x;
	robot_state.left_front_contact.y = forces[0].y;
	robot_state.left_front_contact.z = forces[0].z;
	robot_state.right_front_contact.x = forces[1].x;
	robot_state.right_front_contact.y = forces[1].y;
	robot_state.right_front_contact.z = forces[1].z;
	robot_state.left_rear_contact.x = forces[2].x;
	robot_state.left_rear_contact.y = forces[2].y;
	robot_state.left_rear_contact.z = forces[2].z;
	robot_state.right_rear_contact.x = forces[3].x;
	robot_state.right_rear_contact.y = forces[3].y;
	robot_state.right_rear_contact.z = forces[3].z;

	//grab imu data and world pose
	math::Quaternion imuRot = this->imuSensor->GetOrientation();
	math::Vector3 wLocal = this->imuSensor->GetAngularVelocity();
	math::Vector3 accel = this->imuSensor->GetLinearAcceleration();
	math::Vector3 root_pos = this->model->GetLink("root")->GetWorldPose().pos;

	//reassign imu to msg
	robot_state.orientation.x = imuRot.x;
	robot_state.orientation.y = imuRot.y;
	robot_state.orientation.z = imuRot.z;
	robot_state.orientation.w = imuRot.w;
	robot_state.angular_velocity.x = wLocal.x;
	robot_state.angular_velocity.y = wLocal.y;
	robot_state.angular_velocity.z = wLocal.z;
	robot_state.linear_acceleration.x = accel.x;
	robot_state.linear_acceleration.y = accel.y;
	robot_state.linear_acceleration.z = accel.z;
	robot_state.root_position.x = root_pos.x;
	robot_state.root_position.y = root_pos.y;
	robot_state.root_position.z = root_pos.z;

	statePublisher.publish(robot_state);
}

void SimpleQuadrupedModelPlugin::sumContacts(msgs::Contacts &contacts,
		math::Vector3 &sForce, math::Vector3 &sTorque) {

	for (unsigned int i = 0; i < contacts.contact_size(); ++i) {
		math::Vector3 fTotal;
		math::Vector3 tTotal;
		for (unsigned int j = 0; j < contacts.contact(i).position_size(); ++j) {
			fTotal += math::Vector3(
					contacts.contact(i).wrench(j).body_1_wrench().force().x(),
					contacts.contact(i).wrench(j).body_1_wrench().force().y(),
					contacts.contact(i).wrench(j).body_1_wrench().force().z());
			tTotal += math::Vector3(
					contacts.contact(i).wrench(j).body_1_wrench().torque().x(),
					contacts.contact(i).wrench(j).body_1_wrench().torque().y(),
					contacts.contact(i).wrench(j).body_1_wrench().torque().z());
		}
		// low pass filter over time
		double e = 0.99;
		sForce = sForce * e + fTotal * (1.0 - e);
		sTorque = sTorque * e + tTotal * (1.0 - e);
	}
}

void SimpleQuadrupedModelPlugin::commandCallback(
		simple_quadruped::SimpleQuadrupedCommandConstPtr command_msg) {
	robot_command = *command_msg;
}

void SimpleQuadrupedModelPlugin::InitializeJoints() {
	std::map<std::string, double> initial_state;
	//reset map to defaults
	initial_state.clear();

	initial_state["left_shoulder_x"] = initial_state["right_shoulder_x"] = 0.0;
	initial_state["left_shoulder_y"] = initial_state["right_shoulder_y"] = 1.2;
	initial_state["left_elbow"] = initial_state["right_elbow"] = -2;

	initial_state["left_hip_x"] = initial_state["right_hip_x"] = 0.0;
	initial_state["left_hip_y"] = initial_state["right_hip_y"] = -1.2;
	initial_state["left_knee"] = initial_state["right_knee"] = 2;

	//pause the robot, set the joint positions, unpause.
	bool paused = this->model->GetWorld()->IsPaused();
	this->model->GetWorld()->SetPaused(true);
	this->model->SetJointPositions(initial_state);
	this->model->GetWorld()->SetPaused(paused);
}

GZ_REGISTER_MODEL_PLUGIN (SimpleQuadrupedModelPlugin);
}

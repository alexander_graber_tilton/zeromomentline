/* 
 * File:   simple_quadruped_model_plugin.h
 * Author: alexander graber-tilton
 *
 * Created on February 18, 2014, 6:14 PM
 */

#ifndef SIMPLE_QUADRUPED_CHARACTERISTICS_H
#define	SIMPLE_QUADRUPED_CHARACTERISTICS_H

#include <vector>
#include <string>

class SimpleQuadrupedCharacteristics {
public:
	static const std::vector<std::string> joint_names;
};

#endif

/* 
 * File:   simple_quadruped_model_plugin.h
 * Author: alexander graber-tilton
 *
 * Created on February 18, 2014, 6:14 PM
 */

#ifndef SIMPLE_QUADRUPED_MODEL_PLUGIN_H
#define	SIMPLE_QUADRUPED_MODEL_PLUGIN_H

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Header.h>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include "gazebo/sensors/Sensor.hh"
#include "gazebo/sensors/SensorTypes.hh"
#include "gazebo/sensors/ContactSensor.hh"
#include <gazebo/sensors/ImuSensor.hh>
#include "gazebo/sensors/SensorManager.hh"
#include <simple_quadruped/SimpleQuadrupedCommand.h>
#include <simple_quadruped/SimpleQuadrupedState.h>
#include "simple_quadruped_characteristics.h"


const int NumJoints = 12;

namespace gazebo
{
  class SimpleQuadrupedModelPlugin: public ModelPlugin
  {
    public: SimpleQuadrupedModelPlugin();
    public: virtual ~SimpleQuadrupedModelPlugin();
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
    public: void OnUpdate();

    private:
      //ROS Nodehandle
      ros::NodeHandle* node;
      ros::Subscriber commandSubscriber;
      ros::Publisher statePublisher;
            
      // Pointer to the update event connection
      event::ConnectionPtr updateConnection;
      physics::ModelPtr model;

      //Joints
      std::vector<std::string> joint_names;
      void InitializeJoints();

      //IMU
      sensors::ImuSensorPtr imuSensor;
      std::string imuLinkName;
      void getIMUState(const common::Time &_curTime);

      // Contact sensor
      std::vector<std::string> contactSensorPlacements;
      std::vector<event::ConnectionPtr> contactConnections;
      std::vector<sensors::ContactSensorPtr> contactSensors;
      std::vector<math::Vector3> forces;
      std::vector<math::Vector3> torques;
      void sumContacts(msgs::Contacts& contacts, math::Vector3& sForce, math::Vector3& sTorque);

      //Robot State
      simple_quadruped::SimpleQuadrupedState robot_state;
      
      //Command
      simple_quadruped::SimpleQuadrupedCommand robot_command;
      void commandCallback(simple_quadruped::SimpleQuadrupedCommandConstPtr command_msg);
  };
}


#endif	/* QUADRUPED_MODEL_PLUGIN_H */      
